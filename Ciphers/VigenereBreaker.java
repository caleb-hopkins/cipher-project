package VigenereProgram;

import java.util.*;
import edu.duke.*;
import java.io.*;

public class VigenereBreaker {
    HashMap<String,HashSet<String>> dictionaries;
    public VigenereBreaker(){
        dictionaries = new HashMap<>();
    }
    
    public void importDicts(){
        DirectoryResource dr = new DirectoryResource();
        for(File f : dr.selectedFiles()){
            FileResource fr = new FileResource(f);
            HashSet<String> set = readDict(fr);
            String name = f.getName();
            dictionaries.put(name,set);
        }
    }
    
    public void testDicts(){
        importDicts();
        for(Map.Entry<String,HashSet<String>> e : dictionaries.entrySet()){
            System.out.println(e.getKey()+"\t"+e.getValue().size());
        }
    }
    
    public String sliceString(String message, int whichSlice, int totalSlices) {
        StringBuilder sb = new StringBuilder();
        for (int i = whichSlice;i<message.length();i+=totalSlices){
            char c = message.charAt(i);
            sb.append(c);
        }
        return sb.toString();
    }

    public int[] tryKeyLength(String encrypted, int klength, char mostCommon) {
        CaesarCracker cc = new CaesarCracker(mostCommon); 
        int[] key = new int[klength];
        int i = 0;
        for (int x : key){
            String slice = sliceString(encrypted,i,klength);
            int k = cc.getKey(slice);
            key[i] = k;
            i++;
        }
        return key;
    }
    
    public void testTKL(int length, char mc){
        FileResource fr = new FileResource();
        String s = fr.asString();
        int[] keyArr = tryKeyLength(s,length,mc);
        for(int x : keyArr){
            System.out.println(x);
        }
    }

    public String breakVigenere (int length, char mc, String s) {
        int[] KeyArr = tryKeyLength(s,length,mc);
        VigenereCipher vc = new VigenereCipher(KeyArr);
        String message = vc.decrypt(s);
        return message;
    }
    
    public HashSet<String> readDict(FileResource fr){
        HashSet<String> set = new HashSet();
        for(String e : fr.lines()){
            e = e.toLowerCase();
            set.add(e);
        }
        return set;
    }
    
    public int countWords(String m, HashSet<String> d){
        int i = 0;
        for(String w : m.split("\\W")){
            w = w.toLowerCase();
            if(d.contains(w)){i++;}
        }
        return i;
    }
    
    public String breakForLanguage(String e, HashSet<String> d, char mc){
        String broken = "";
        int max = 0;
        for(int i = 1;i<100;i++){
            String s = breakVigenere(i,mc,e);
            int count = countWords(s,d);
            if(count>max){max = count;
                          broken = s;
            }
        }
        System.out.println(max);
        return broken;
    }
    
    public char mostCommonChar(HashSet<String> d){
        HashMap<Character,Integer> let = new HashMap<>();
        for(String s : d){
            for(char c : s.toCharArray()){
                if(!let.containsKey(c)){
                    let.put(c,1);
                }else{
                    int val = let.get(c);
                    let.put(c,val+1);
                }
            }
        }
        int max = 0;
        char mc = '#';
        for(Map.Entry<Character,Integer> k : let.entrySet()){
            int val = k.getValue();
            if(val>max){
                max = val;
                mc = k.getKey();
            }
        }
        return mc;
    }
    
    public void breakForAllLangs(String e, HashMap<String,HashSet<String>> d){
        for (Map.Entry<String,HashSet<String>> h : dictionaries.entrySet()){
            char mc = mostCommonChar(h.getValue());
            System.out.println(h.getKey());
            String a = breakForLanguage(e,h.getValue(),mc);
        }
    }
    
    public void testAllLangs(){
        importDicts();
        FileResource fr = new FileResource();
        String s = fr.asString();
        breakForAllLangs(s,dictionaries);
    }
    
    public void testBreaker(char mc){
        FileResource dict = new FileResource();
        HashSet<String> d = readDict(dict);
        System.out.println("Library Read");
        FileResource fr = new FileResource();
        String s = fr.asString();
        String decrypted = breakForLanguage(s,d,mc);
        System.out.println(decrypted);
    }
}
